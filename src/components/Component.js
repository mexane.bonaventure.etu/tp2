class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	render(){ // déclaration de méthode
		if(this.children != null && this.children != undefined) {
			return `<${this.tagName}>${this.children}</${this.tagName}>`;
		} else {
			return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"/>`;
		}
	}
}
export default Component;