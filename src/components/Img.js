import Component from './Component.js';
class Img extends Component {
	constructor(url) {
		super('img', { name: 'src', value: url });
	}
}
export default Img;